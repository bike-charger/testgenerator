var express = require('express');
var app = express();

var cors = require('cors');

app.use(cors());

app.get('/rpm', function(req, res) {
    res.json({
        rpm: random(700, 4000)
    });
});

app.listen(3001);

function random(start, end) {
    return Math.floor(Math.random() * Math.floor(end-start+1)) + start;
}